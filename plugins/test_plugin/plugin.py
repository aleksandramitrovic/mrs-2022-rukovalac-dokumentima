from plugin_framework.extension import Extension
from .widget import TextEdit

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = None
        self.iface = iface
        print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        self.widget = TextEdit(self.iface.central_widget)
        self.iface.setCentralWidget(self.widget)
        print("Activated")

    def deactivate(self):
        # TODO: ukloniti TextEdit sa centralnog widget-a.
        print("Deactivated")